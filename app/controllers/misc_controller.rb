class MiscController < ApplicationController

  def dummy_method
  end

  def dummy_save
    redirect_to articles_path, notice: "Saved changes"
  end

  def dummy_add
    redirect_to articles_path, notice: "Added"
  end

end
