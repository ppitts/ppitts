class ArticlesController < ApplicationController
    
  def index
    @articles = Article.all
    
    respond_to do |format|
      format.html
      #format.csv { render text: @articles.to_csv }
      format.csv { send_data @articles.to_csv, :filename => 'users.csv' }
      #format.csv { send_data @articles.to_csv }
      #format.xls { send_data @articles.to_csv(col_sep: ",") }
      #format.xls { send_data @articles.to_csv(col_sep: "\t") }
      format.xls { send_data @articles.to_csv(col_sep: "\t"), :filename => 'users.xls' }
      
      #format.pdf do ||
	  #pdf = PDF::Writer.new
	  #pdf.text = "test pdf"
	  #send_data pdf.render, :filename => 'users.pdf', :type => 'application/pdf', :disposition => 			'inline'
      #end
    end
  end
  
  def show
    @article = Article.find(params[:id])
  end
  
  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end
  
  def create
    #@article = Article.new(article_params)
    #if @article.save
    #  redirect_to @article, notice: "Successfully created user."
    #else
    #  render 'new'
    #end
    
    @article = Article.new(article_params)
    @article.save
    
    respond_to do |format|
      #format.html { redirect_to root_url }
      format.html { redirect_to @article, notice: "Successfully created user." }
      format.js
    end
  end

  def update
    @article = Article.find(params[:id])
 
    #original code
    #if @article.update(article_params)
    #  redirect_to @article
    #else
    #  render 'edit'
    #end
    
    #from tutorial
    #@article.update(article_params)
    #respond_with @article
    #requires respond_to at top of controller
    #respond_to :html, :json
    
    respond_to do |format|
     if @article.update(article_params)
        format.html
	format.xml { render :xml => @article }
	format.json { render :json => @article }
#      else
#        format.html { render :action => "edit" }
#        format.json { render :json => @articles.errors.full_messages, :status => #:unprocessable_entity }
      end
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
     redirect_to articles_path, notice: "Successfully destroyed user."
  end

  private
    def article_params
      params.require(:article).permit(:name, :role, :oncall, :locale, :notes)
    end
end
