class Article < ActiveRecord::Base

  #attr_accessible :name, :role, :oncall, :locale, :notes
  
  #validates :title, presence: true, length: { minimum: 5 }
  #validates :text, presence: true, length: { minimum: 1 }
  validates :name, presence: true, length: { minimum: 3 }
  validates :role, presence: true, length: { minimum: 3 }
  validates :oncall, inclusion: { in: [true, false] }
  #validates :oncall, inclusion: { in: [nil] }
  validates :locale, inclusion: { in: %w(nv md china) }
  #validates :locale, presence: true, length: { minimum: 2 }
  validates :notes, presence: true, length: { minimum: 2 }
  
  def self.to_csv(options = {})
    CSV.generate(options) do|csv|
      #csv << column_names
      csv << ['Name', 'Role', 'Oncall', 'Locale', 'Notes']
      all.each do |article|
	#csv << article.attributes.values_at(*column_names)
	csv << [article.name, article.role, article.oncall, article.locale, article.notes]
      end
    end
  end
  
end
